============ BUNDLER.JS ============

Registra uma função na coleção:
> bundler.register( "NomeDaView" , funcao [, ( 1:fim | -1:início ) ]);

Executa a coleção na ordem de inclusão:
> bundler.exec( "NomeDaView" [, ..."Outras Views" ] );

Exibe as funções da coleção:
> bundler.collection("NomeDaView" [, ordem ] );

Exibe a ordem de execução de coleções atual com sua ordem:
> bundler.log();

====================================