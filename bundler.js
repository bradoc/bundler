var bundler = function() {
    var proto = { // private
        chunks:[],
        helps:[ // tente: bundler.help() no console do browser
            'Registra uma função na coleção',
            'bundler.register( "NomeDaView" , funcao [, ( 1:fim | -1:início ) ]);',
            'Executa a coleção na ordem de inclusão',
            'bundler.exec( "NomeDaView" [, ..."Outras Views" ] );',
            'Exibe as funções da coleção',
            'bundler.collection("NomeDaView" [, ordem ] );',
            'Exibe a ordem de execução de coleções atual com sua ordem',
            'bundler.log();'
        ],
        setDevMode:function(bool) { this.isDevMode = bool != undefined && bool; } // desliga o devMode, mas procure não usar. Há cálculo automático disso (linha 55).
    }

    var bundler = { // public
        isDevMode:true,
        executionOrder:[],
        help:function() {
            var cons = this.helps.map(function(obj,index) {
                return obj+(index%2?'\n\n':':\n> ')
            }).join('');
            console.log([
                '\n\n\n\n\n\n============ BUNDLER.JS ============\n\n',
                cons,
                '====================================\n\n\n\n\n\n'
            ].join(''));
        },
        exec:function(name) {
            var _this = this, order = this.executionOrder;
            var list = Array.isArray(name)?name:list = [].slice.call(arguments);
            list.map(function(chunk) {
                var collection = _this.chunks[chunk].collection;
                var last =  collection[collection.length-1];
                order.push({collection:chunk,executionOrder:order.length,lambda:last.code});
                last.lambda();
            });
            if(this.isDevMode) console.trace();
        },
        register:function(name,fn,order) {
            this.chunks[name] = this.chunks[name] || {name:name,collection:[]}
            var description = {
                code:fn.toString(),
                lambda:fn
            }
            this.chunks[name].collection[order==-1?'unshift':'push'](description); // adiciona inicio ou final da collection
        },
        collection:function(name,order) {
            var chunk = this.chunks[name];
            if(order != undefined) return { fn:chunk.collection[parseInt(order)],order:order}
            return chunk.collection.map(function(obj,index) {return{ fn:obj, order:index }});
        },
        log:function() { console.log(this.executionOrder); }
    };
    bundler.__proto__ = proto;

    try { // bundler nunca em dev quando em producao. location dá throw error no nodejs, por isso o try.
        if(!location.href.match(/(localhost|homolog)/)) bundler.isDevMode = false;
    } catch(e) {}
    return bundler;
}
try { // global (browser)
    window.bundler = bundler();
} catch(e) { // ou módulo (nodejs)
    bundler = bundler();
    module.exports = bundler;
}

/* TESTES */
var local = {};
bundler.register('root',function() {
    local.root = 'valor 1';
});
bundler.register('root',function() {
    local.root = 'valor 2'
});
bundler.register('modelos',function() {
    local.modelos = 'valor 1'
});
bundler.register('eventos',function() {
    local.eventos = 'valor 1';
});
bundler.collection('root');
bundler.exec(['root','modelos','eventos']);
bundler.log();
local /*?*/
//bundler.help();